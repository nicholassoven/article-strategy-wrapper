package save_person

import "gitlab.com/nicholassoven/article-strategy-wrapper/app"

type Builder struct {
	service app.SavePersonService
}

func BuildIdleService() *Builder {
	return &Builder{
		service: app.NoSavePersonService,
	}
}

func (b Builder) SavePerson(id int, details app.PersonDetails) error {
	return b.service.SavePerson(id, details)
}

func (b *Builder) WithSavingPersonIntoRepository(repo PersonRepository) *Builder {
	b.service = withSavingPersonIntoRepository(b.service, repo)
	return b
}

func (b *Builder) WithPreValidatingPerson(validator PersonValidator) *Builder {
	b.service = withPreValidatingPerson(b.service, validator)
	return b
}

func (b *Builder) WithMetricSending(metricSender MetricSender, metricName string) *Builder {
	b.service = withMetricSending(b.service, metricSender, metricName)
	return b
}
