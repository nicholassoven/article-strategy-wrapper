package save_person

import (
	"time"

	"github.com/pkg/errors"

	"gitlab.com/nicholassoven/article-strategy-wrapper/app"
)

type MetricSender interface {
	SendDurationMetric(metricName string, d time.Duration)
}

type SendMetricService struct {
	base         app.SavePersonService
	metricSender MetricSender
	metricName   string
}

func withMetricSending(base app.SavePersonService, metricSender MetricSender, metricName string) SendMetricService {
	return SendMetricService{base: base, metricSender: metricSender, metricName: metricName}
}

func (s SendMetricService) SavePerson(id int, details app.PersonDetails) error {
	startTime := time.Now()

	err := s.base.SavePerson(id, details)

	s.metricSender.SendDurationMetric(s.metricName, time.Since(startTime))

	if err != nil {
		return errors.Wrap(err, "save person in base in sending metric service")
	}

	return nil
}
