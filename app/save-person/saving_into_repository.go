package save_person

import (
	"github.com/pkg/errors"

	"gitlab.com/nicholassoven/article-strategy-wrapper/app"
)

type PersonRepository interface {
	UpdatePerson(id int, details app.PersonDetails) error
}

type SavePersonIntoRepositoryService struct {
	base app.SavePersonService
	repo PersonRepository
}

func withSavingPersonIntoRepository(base app.SavePersonService, repo PersonRepository) SavePersonIntoRepositoryService {
	return SavePersonIntoRepositoryService{base: base, repo: repo}
}

func (s SavePersonIntoRepositoryService) SavePerson(id int, details app.PersonDetails) error {
	err := s.base.SavePerson(id, details)
	if err != nil {
		return errors.Wrap(err, "save person in base in save person into repository service")
	}

	err = s.repo.UpdatePerson(id, details)
	if err != nil {
		return errors.Wrap(err, "update person in repo")
	}

	return nil
}
