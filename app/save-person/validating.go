package save_person

import (
	"github.com/pkg/errors"

	"gitlab.com/nicholassoven/article-strategy-wrapper/app"
)

type PersonValidator interface {
	ValidatePerson(details app.PersonDetails) error
}

type PreValidatePersonService struct {
	base      app.SavePersonService
	validator PersonValidator
}

func withPreValidatingPerson(base app.SavePersonService, validator PersonValidator) PreValidatePersonService {
	return PreValidatePersonService{base: base, validator: validator}
}

func (s PreValidatePersonService) SavePerson(id int, details app.PersonDetails) error {
	err := s.validator.ValidatePerson(details)
	if err != nil {
		return errors.Wrap(err, "validate person")
	}

	err = s.base.SavePerson(id, details)
	if err != nil {
		return errors.Wrap(err, "save person in base in pre validate person service")
	}

	return nil
}
