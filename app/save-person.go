package app

type SavePersonService interface {
	SavePerson(id int, details PersonDetails) error
}

type noSavePersonService struct{}

func (noSavePersonService) SavePerson(_ int, _ PersonDetails) error { return nil }

var NoSavePersonService = noSavePersonService{}
