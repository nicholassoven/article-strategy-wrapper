package main

import (
	"log"

	"github.com/pkg/errors"

	"gitlab.com/nicholassoven/article-strategy-wrapper/app"
	savePerson "gitlab.com/nicholassoven/article-strategy-wrapper/app/save-person"
)

func main() {
	err := run()
	if err != nil {
		log.Fatalf("run: %+v", err)
	}
}

func NewPostgreSQLDatabaseClient(dsn string) savePerson.PersonRepository {
	_ = dsn // TODO implement
	panic("not implemented")
}

func NewMemoryCache() savePerson.PersonRepository {
	// TODO implement
	panic("not implemented")
}

func NewMongoDBClient(dsn string) savePerson.PersonRepository {
	_ = dsn // TODO implement
	panic("not implemented")
}

type personAgeValidator struct{}

func (personAgeValidator) ValidatePerson(details app.PersonDetails) error {
	if details.Age < 18 {
		return errors.New("invalid age")
	}
	return nil
}

var PersonAgeValidator = personAgeValidator{}

func MetricSender() savePerson.MetricSender {
	// TODO implement
	panic("not implemented")
}

func run() error {
	userService := savePerson.BuildIdleService().
		WithSavingPersonIntoRepository(NewPostgreSQLDatabaseClient("postgres://user:pass@127.0.0.1:5432/platform?sslmode=disable")).
		WithMetricSending(MetricSender(), "save-into-postgresql-duration").
		WithSavingPersonIntoRepository(NewMemoryCache())

	err := userService.SavePerson(5, app.PersonDetails{
		Name: "Mary",
		Age:  17,
	})
	if err != nil {
		return errors.Wrap(err, "save user Mary")
	}

	taxpayerService := savePerson.BuildIdleService().
		WithSavingPersonIntoRepository(NewMongoDBClient("mongodb://user:pass@127.0.0.1:27017/tax_system")).
		WithMetricSending(MetricSender(), "save-into-mongodb-duration").
		WithSavingPersonIntoRepository(NewMemoryCache()).
		WithPreValidatingPerson(PersonAgeValidator).
		WithMetricSending(MetricSender(), "save-taxpayer-duration")

	err = taxpayerService.SavePerson(1326423, app.PersonDetails{
		Name: "Jack",
		Age:  37,
	})
	if err != nil {
		return errors.Wrap(err, "save taxpayer Jack")
	}

	return nil
}
